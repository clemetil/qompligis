"""
This file contains the report class
"""

import json
from enum import Enum, unique
from typing import Any, Dict, List, Union

from QompliGIS.__about__ import DIR_PLUGIN_ROOT_RESOURCES
from QompliGIS.qt_utils import tr


@unique
class ReportFormat(Enum):
    """Report Format."""

    HTML = 0
    MARKDOWN = 1
    JSON = 2


@unique
class LayerSection(Enum):
    """Layer Section."""

    CRS = 0
    FIELDS = 1
    CURVES = 2
    HOLES = 3
    MIN_LENGTH = 4
    MIN_AREA = 5
    INVALID_GEOMETRY = 6
    NULL_GEOMETRY = 7
    EMPTY_GEOMETRY = 8
    DUPLICATES = 9
    OVERLAPS = 10


@unique
class MainSection(Enum):
    """Main Section."""

    RESULT = 0
    LAYER_LIST = 1


class QompliGISReport:
    """QompliGIS Report."""

    def __init__(self):
        # Must be in the same order than the enums above
        self.main_sections = [tr("Result"), tr("Layer list")]
        self.layer_sections = [
            tr("CRS"),
            tr("Fields"),
            tr("Curves"),
            tr("Holes"),
            tr("Minimum length"),
            tr("Minimum area"),
            tr("Invalid geometries"),
            tr("Null geometries"),
            tr("Empty geometries"),
            tr("Duplicate geometries"),
            tr("Overlaps geometries"),
        ]
        self.layers = []
        self.report_dict: Dict[str, Any] = {
            main_section: list() for main_section in self.main_sections
        }

    def addLayer(self, layer_name: str) -> None:
        """add layer layer_name to the report.

        :param layer_name: name of the layer
        :type layer_name: str
        :rtype: None
        """
        self.report_dict[layer_name] = {section: [] for section in self.layer_sections}
        self.report_dict[layer_name][
            self.layer_sections[LayerSection.FIELDS.value]
        ] = {}

    def addLayerSection(
        self, layer_name: str, layer_section: LayerSection, info: str
    ) -> None:
        """add a layer section.

        :param layer_name: name of the layer
        :type layer_name: str
        :param layer_section: section for the layer
        :type layer_section: LayerSection
        :param info: information for the section
        :type info: str
        :rtype: None
        """
        if layer_section == LayerSection.FIELDS:
            raise ValueError(tr("For FIELDS section, use addFieldInfo() method"))
        try:
            self.report_dict[layer_name][
                self.layer_sections[layer_section.value]
            ].append(info)
        except KeyError as err:
            raise KeyError(
                tr("The layer {0} is not present in the report").format(layer_name)
            ) from err

    def addMainSection(self, section: MainSection, info: str) -> None:
        """add a main section.

        :param section: section of the report
        :type section: MainSection
        :param info: information for the section
        :type info: str
        :rtype: None
        """
        self.report_dict[self.main_sections[section.value]].append(info)

    def addFieldInfo(self, layer_name: str, field_name: str, info: str) -> None:
        """add field information.

        :param layer_name: name of the layer
        :type layer_name: str
        :param field_name: name of the field
        :type field_name: str
        :param info: information for the field
        :type info: str
        :rtype: None
        """
        fields_dict = self.report_dict[layer_name][
            self.layer_sections[LayerSection.FIELDS.value]
        ]
        try:
            fields_dict[field_name].append(info)
        except KeyError:
            fields_dict[field_name] = [info]

    def report(
        self, report_format: ReportFormat, css_path: str = ""
    ) -> Union[dict, str]:
        """Generate the report.

        :param report_format: report format
        :param css_path: path of the css file
        :type report_format: ReportFormat
        :rtype: Union[dict, str]
        """
        report = ""
        if report_format == ReportFormat.JSON:
            return json.dumps(self.report_dict, ensure_ascii=False)
        if report_format == ReportFormat.HTML:
            report += formatHead(css_path)
            report += formatNav(self)

        for key, value in self.report_dict.items():
            report += formatTitle(report_format, key)
            if key in self.main_sections:
                report += formatContent(report_format, value)
            else:
                for layer_section in self.layer_sections:
                    if len(value[layer_section]) == 0:
                        continue
                    report += formatSection(report_format, layer_section)
                    if layer_section == self.layer_sections[LayerSection.FIELDS.value]:
                        if "" in value[layer_section]:
                            info = value[layer_section].pop("")
                            report += formatContent(report_format, info)
                        for field, info in value[layer_section].items():
                            report += formatField(report_format, field)
                            report += formatContent(report_format, info)
                    else:
                        report += formatContent(report_format, value[layer_section])

        return report


def formatNav(self) -> str:
    """format navigation.

    :rtype: str
    """
    res = '<ul class="nav">'
    for key, value in self.report_dict.items():
        if key not in self.main_sections:
            res += '<li><a href="#' + key + '">' + key + "</a></li>"
    res += "</ul>"
    return res


def formatHead(css_path: str) -> str:
    """format head.

    :param css_path: path of the css file
    :type css_path: str
    :rtype: str
    """
    res = '<head><link rel="stylesheet" href="'
    if css_path == "":
        res += str(DIR_PLUGIN_ROOT_RESOURCES) + "/css/report_html.css"
    else:
        res += css_path
    return (
        res
        + '"><link rel="icon" type="image/png" href="'
        + str(DIR_PLUGIN_ROOT_RESOURCES)
        + '/images/qompligis_logo.png"><title>'
        + tr("Report")
        + '</title><meta charset="utf-8"></head>\
        <div id="logo"><img id="logo-oslandia" src="'
        + str(DIR_PLUGIN_ROOT_RESOURCES)
        + '/images/logo_oslandia.png">\
        <img id="logo-qompligis" src="'
        + str(DIR_PLUGIN_ROOT_RESOURCES)
        + '/images/qompligis_logo.png"></div>\
        <h1 id="title">'
        + tr("Compliance Report")
        + "</h1>"
    )


def formatTitle(report_format: ReportFormat, text: str) -> str:
    """format title.

    :param report_format: report format
    :type report_format: ReportFormat
    :param text: report title
    :type text: str
    :rtype: str
    """
    if report_format == ReportFormat.HTML:
        return '<h1 id="' + text + '">' + text + "</h1>"
    if report_format == ReportFormat.MARKDOWN:
        return "# " + text + formatNewLine(report_format)
    raise NotImplementedError()


def formatSection(report_format: ReportFormat, text: str) -> str:
    """format section.

    :param report_format: report format
    :type report_format: ReportFormat
    :param text: section text
    :type text: str
    :rtype: str
    """
    if report_format == ReportFormat.HTML:
        return "<h2>" + text + "</h2>"
    if report_format == ReportFormat.MARKDOWN:
        return "## " + text + formatNewLine(report_format)
    raise NotImplementedError()


def formatField(report_format: ReportFormat, text: str) -> str:
    """format field.

    :param report_format: report format
    :type report_format: ReportFormat
    :param text: field text
    :type text: str
    :rtype: str
    """
    if report_format == ReportFormat.HTML:
        return "<h3>" + text + "</h3>"
    if report_format == ReportFormat.MARKDOWN:
        return "### " + text + formatNewLine(report_format)
    raise NotImplementedError()


def formatContent(report_format: ReportFormat, text: List[str]) -> str:
    """format content.

    :param report_format: report format
    :type report_format: ReportFormat
    :param text: text list to format
    :type text: List[str]
    :rtype: str
    """
    if report_format == ReportFormat.HTML:
        return "<p>" + formatNewLine(report_format).join(text) + "</p>"
    if report_format == ReportFormat.MARKDOWN:
        return formatNewLine(report_format).join(text) + formatNewLine(report_format)
    raise NotImplementedError()


def formatBold(report_format: ReportFormat, text: str) -> str:
    """Returns a bold part for the specified text.

    :param report_format: report format
    :type report_format: ReportFormat
    :param text:
    :type text: str
    :rtype: str
    """
    if report_format == ReportFormat.HTML:
        return "<bold>" + text + "</bold>"
    if report_format == ReportFormat.MARKDOWN:
        return "**" + text + "**"
    raise NotImplementedError()


def formatNewLine(report_format: ReportFormat) -> str:
    """Returns the new line character for the desired format

    :param report_format: report format
    :type report_format: ReportFormat
    :rtype: str
    """
    if report_format == ReportFormat.HTML:
        return "<br>"
    if report_format == ReportFormat.MARKDOWN:
        return "\n\n"
    raise NotImplementedError()
